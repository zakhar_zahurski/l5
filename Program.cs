﻿using System;

namespace Lesson_5
{
    class Program 
    {
        static void Main(string[] args)
        {
            Sum();
            RectangularArea();
            CircleArea();
            Even();
            ArraySum();
        }
        static void Sum()
        {
            Console.WriteLine("=====1.Сумма двух чисел=====\nВведите 1 число:");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите 2 число:");
            int b = Convert.ToInt32(Console.ReadLine());
            int c = a + b;
            Console.WriteLine("Сумма " + c);
        }
        static void RectangularArea()
        {
            Console.WriteLine("\n=====2.Площадь прямоугольника=====\nДлина прямоугольника:");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ширина прямоугольника:");
            int b = Convert.ToInt32(Console.ReadLine());
            int c = a * b;
            Console.WriteLine("Площадь " + c);
        }
        static void CircleArea()
        {
            Console.WriteLine("\n=====3.Площадь круга=====\nВведите радиус круга");
            double a = Convert.ToInt32(Console.ReadLine());
            double b = Math.PI * (a*a);
            Console.WriteLine("Площадь " + b);
        }
        static void Even()
        {
            Console.WriteLine("\n=====4.Четное/нечетное=====\nВведите число");
            int a = Convert.ToInt32(Console.ReadLine());
            if (a % 2 == 0)
            {
                Console.WriteLine("Число" + a + "четное");
            }
            else
            {
                Console.WriteLine("Число " + a + " нечетное");
            }

        }

        static void ArraySum()
        {
            Console.WriteLine("\n=====5.Сумма элементов масива=====\nВведите количество элементов массива");
            int a = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[a];
            int b = 0;
            Console.WriteLine("Введите числа в массив");
            for (int f = 0; f < array.Length; f++)
            {
                int c = Convert.ToInt32(Console.ReadLine());
                array[f] = c;
            }
            

            for(int i = 0; i < array.Length; i++)
            {
                b += array[i];
            }

            Console.WriteLine("Сумма элементов массива равна: " + b);
        }
    }
}